﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto_SGPF.Models
{
    public class Productos
    {
        [Key]
        public int IdProducto { get; set; }
        [Required]
        [StringLength(64)]
        public string Nombre { get; set; }
        [Required]
        [StringLength(30)]
        public string Tamanio { get; set; }
        [Required]
        public int Pisos { get; set; }
        [Required]
        [StringLength(250)]
        public string Ingredientes { get; set; }
        [Required]
        public double Precio { get; set; }
        [Required]
        public bool Estatus { get; set; }
        public virtual List<Ventas> Venta { get; set; }
    }
}