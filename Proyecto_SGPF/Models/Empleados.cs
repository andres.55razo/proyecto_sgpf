﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto_SGPF.Models
{
    public class Empleados
    {
        [Key]
        [StringLength(64)]
        public string IdEmpleado { get; set; }
        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }
        [Required]
        [StringLength(100)]
        public string ApellidoPaterno { get; set; }
        [Required]
        [StringLength(100)]
        public string ApellidoMaterno { get; set; }
        [Required]
        public int Puesto { get; set; }
        [Required]
        public bool Genero { get; set; }
        [StringLength(120)]
        public string Domicilio { get; set; }
        [Required]
        [StringLength(14)]
        public string Rfc { get; set; }
        [Required]
        [StringLength(15)]
        public string Telefono { get; set; }
        [Required]
        [StringLength(64)]
        public string Email { get; set; }
        [Required]
        public bool Estatus { get; set; }
        [Required]
        [StringLength(64)]
        public string Usuario { get; set; }
        [Required]
        [StringLength(64)]
        public string Password { get; set; }
        public virtual Sucursales Sucursal { get; set; }
    }
}