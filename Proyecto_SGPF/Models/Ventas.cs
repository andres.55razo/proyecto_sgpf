﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto_SGPF.Models
{
    public class Ventas
    {
        [Key]
        public int IdVentas{ get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }
        [Required]
        public bool Estatus { get; set; }
        public virtual Clientes Cliente { get; set; }
        public virtual List<Productos> Producto { get; set; }

        public Ventas(int idVentas, DateTime fecha, bool estatus, Clientes cliente, List<Productos> producto)
        {
            IdVentas = idVentas;
            Fecha = fecha;
            Estatus = estatus;
            Cliente = cliente;
            Producto = producto;
        }

        public Ventas()
        {
        }
    }
}