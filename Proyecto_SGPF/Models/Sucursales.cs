﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto_SGPF.Models
{
    public class Sucursales
    {
        [Key]
        public int IdSucursal { get; set; }
        [Required]
        [StringLength(64)]
        public string Nombre { get; set; }
        [Required]
        [StringLength(120)]
        public string Direccion { get; set; }
        [Required]
        [StringLength(15)]
        public string Telefono { get; set; }
        [Required]
        [StringLength(64)]
        public string Email { get; set; }
        [Required]
        public bool Estatus { get; set; }
        public virtual List<Empleados> Empleado { get; set; }
    }
}