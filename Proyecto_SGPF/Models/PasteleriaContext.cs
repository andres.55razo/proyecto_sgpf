﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Proyecto_SGPF.Models
{
    public class PasteleriaContext: DbContext
    {
        private const string NameOrConnectionString = "DefaultConnection";
        public PasteleriaContext() : base(NameOrConnectionString)
        {

        }

        public DbSet<Clientes> Clientes { get; set; }
        public DbSet<Empleados> Empleados { get; set; }
        public DbSet<Productos> Productos { get; set; }
        public DbSet<Proveedores> Proveedores { get; set; }
        public DbSet<Sucursales> Sucursales { get; set; }
        public DbSet<Ventas> Ventas { get; set; }
    }
}