﻿using Proyecto_SGPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto_SGPF.Services
{
    public class LoginServices
    {
        public List<Empleados> VerificarUsuario(Empleados empleado)
        {
            using (PasteleriaContext dbModel = new PasteleriaContext())
            {
                var consulta = from dato in dbModel.Empleados
                               where dato.Usuario == empleado.Usuario && dato.Password == empleado.Password && dato.Estatus == true
                               select dato;
                return consulta.ToList();
            }
        }
    }
}