﻿using Proyecto_SGPF.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto_SGPF.Controllers
{
    public class ClientesController : Controller
    {
        // GET: Clientes
        public ActionResult Index()
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Clientes.Where(x => x.Estatus == true).ToList());
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Clientes cliente)
        {
            try
            {
                cliente.Estatus = true;
                cliente.IdCliente = DateTime.Now.ToString("yyyMMddhhmmss") +
                    cliente.Nombre.ToLower();
                cliente.IdCliente= cliente.IdCliente.Replace("á","a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").
                    Replace("ú", "u").Replace("ü", "u").Replace("ñ", "n");

                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Clientes.Add(cliente);
                    dbmodel.SaveChanges();
                }

                return RedirectToAction("Index", "Clientes");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(string id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Clientes.Where(x => x.IdCliente == id).FirstOrDefault());
            }
        }

        [HttpPost]
        public ActionResult Edit(string id, Clientes cliente)
        {
            try
            {
                cliente.IdCliente = id;
                cliente.Estatus = true;
                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Entry(cliente).State = EntityState.Modified;
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index","Clientes");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Details(string id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Clientes.Where(x => x.IdCliente == id).FirstOrDefault());
            }
        }

        public ActionResult Delete(string id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Clientes.Where(x => x.IdCliente == id).FirstOrDefault());
            }
        }

        [HttpPost]
        public ActionResult Delete(string id, Clientes cliente)
        {
            try
            {
                cliente.IdCliente = id;
                cliente.Estatus = false;
                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Entry(cliente).State = EntityState.Modified;
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index", "Clientes");
            }
            catch
            {
                return View();
            }
        }
    }
}