﻿using Proyecto_SGPF.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto_SGPF.Controllers
{
    public class ProveedoresController : Controller
    {
        // GET: Proveedores
        public ActionResult Index()
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Proveedores.Where(x => x.Estatus == true).ToList());
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Proveedores proveedor)
        {
            try
            {
                proveedor.Estatus = true;
                proveedor.IdProveedor = DateTime.Now.ToString("yyyMMddhhmmss") +
                    proveedor.Nombre.ToLower();
                proveedor.IdProveedor = proveedor.IdProveedor.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").
                    Replace("ú", "u").Replace("ü", "u").Replace("ñ", "n");

                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Proveedores.Add(proveedor);
                    dbmodel.SaveChanges();
                }

                return RedirectToAction("Index", "Proveedores");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(string id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Proveedores.Where(x => x.IdProveedor == id).FirstOrDefault());
            }
        }

        [HttpPost]
        public ActionResult Edit(string id, Proveedores proveedor)
        {
            try
            {
                proveedor.IdProveedor = id;
                proveedor.Estatus = true;
                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Entry(proveedor).State = EntityState.Modified;
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index", "Proveedores");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Details(string id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Proveedores.Where(x => x.IdProveedor == id).FirstOrDefault());
            }
        }

        public ActionResult Delete(string id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Proveedores.Where(x => x.IdProveedor == id).FirstOrDefault());
            }
        }

        [HttpPost]
        public ActionResult Delete(string id, Proveedores proveedor)
        {
            try
            {
                proveedor.IdProveedor = id;
                proveedor.Estatus = false;
                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Entry(proveedor).State = EntityState.Modified;
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index", "Proveedores");
            }
            catch
            {
                return View();
            }
        }
    }
}