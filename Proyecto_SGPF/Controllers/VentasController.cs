﻿using Proyecto_SGPF.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto_SGPF.Controllers
{
    public class VentasController : Controller
    {
        // GET: Ventas
        public ActionResult Index()
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Ventas.Where(x => x.Estatus == true).ToList().OrderBy(x => x.Fecha));
            }
        }

        public ActionResult BuscarClientes()
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Clientes.Where(x => x.Estatus == true).ToList());
            }

        }

        public ActionResult SeleccionPastel(string id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                var consulta = dbmodel.Clientes.Where(x => x.IdCliente == id).ToList();
                List<Clientes> list = new List<Clientes>();
                list = (List<Clientes>)consulta;
                Session["Cliente"] = list.FirstOrDefault();
            }
            return RedirectToAction("ListadoPastel", "Ventas");
        }

        public ActionResult ListadoPastel(int? id)
        {
            if (id != null)
            {
                List<int> codigosPasteles;
                if (Session["Pasteles"] == null)
                {
                    codigosPasteles = new List<int>();
                }
                else
                {
                    codigosPasteles = Session["Pasteles"] as List<int>;
                }
                codigosPasteles.Add(id.Value);
                Session["Pasteles"] = codigosPasteles;
            }
            ViewBag.Favoritos = Session["Pasteles"];
            using (PasteleriaContext dbModel = new PasteleriaContext())
            {
                return View(dbModel.Productos.Where(x => x.Estatus).ToList());
            }
        }

        public ActionResult ListarPastelesAgregado(int? id)
        {
            List<int> codigos = (List<int>)Session["Pasteles"];
            Clientes cliente = (Clientes)Session["Cliente"];
            ViewBag.ClienteVenta = cliente.Nombre;
            if (id != null)
            {
                codigos.Remove(id.Value);
            }

            if (codigos.Count() == 0)
            {
                Session["Pasteles"] = null;
            }
            else
            {
                Session["Pasteles"] = codigos;
            }

            if (Session["Pasteles"] != null)
            {
                using (PasteleriaContext dbModel = new PasteleriaContext())
                {
                    var consulta = from datos in dbModel.Productos where codigos.Contains(datos.IdProducto) select datos;
                    List<Productos> productos = consulta.ToList();

                    ViewBag.Total = productos.Sum(x => x.Precio);

                    return View(productos);
                }
            }
            return View();
        }

        public ActionResult Comprar()
        {
            Ventas venta;
            Clientes cliente = (Clientes)Session["Cliente"];
            Clientes clien;
            List<int> codigos = (List<int>)Session["Pasteles"];

            List<Productos> listaProd = new List<Productos>();
            Productos p = new Productos();

            try
            {
                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    var consulta = from dato in dbmodel.Clientes
                                   where dato.IdCliente == cliente.IdCliente
                                   select dato;
                    clien = consulta.FirstOrDefault();

                    foreach (var item in codigos)
                    {
                        p=dbmodel.Productos.Find(item);
                        listaProd.Add(p);
                    }

                    venta = new Ventas(0,DateTime.Now,true,clien,listaProd);

                    dbmodel.Ventas.Add(venta);
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("ListarPastelesAgregado");
            }
        }

        public ActionResult CancelarVenta()
        {
            return RedirectToAction("Index", "Ventas");
        }
    }
}