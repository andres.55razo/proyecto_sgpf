﻿using Proyecto_SGPF.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto_SGPF.Controllers
{
    public class EmpleadosController : Controller
    {
        // GET: Empleados
        public ActionResult Index()
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Empleados.Where(x => x.Estatus == true).ToList());
            }
        }

        public ActionResult BuscarSucursal()
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Sucursales.Where(x => x.Estatus == true).ToList());
            }
            
        }

        public ActionResult Create(int id)
        {
            Empleados emp = new Empleados();
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                var consulta = from dato in dbmodel.Sucursales
                               where dato.IdSucursal == id
                               select dato;
                emp.Sucursal = consulta.FirstOrDefault();
                return View(emp);
            }
        }

        [HttpPost]
        public ActionResult Create(Empleados empleado)
        {
            try
            {
                empleado.Estatus = true;
                empleado.IdEmpleado = DateTime.Now.ToString("yyyMMddhhmmss") +
                    empleado.Nombre.ToLower();
                empleado.IdEmpleado = empleado.IdEmpleado.Replace("á", "a").Replace("é", "e").Replace("í", "i").Replace("ó", "o").
                    Replace("ú", "u").Replace("ü", "u").Replace("ñ", "n");

                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Empleados.Add(empleado);
                    dbmodel.SaveChanges();
                }

                return RedirectToAction("Index", "Empleados");
            }
            catch
            {
                return View(empleado.Sucursal.IdSucursal);
            }
        }

        public ActionResult BuscarSucursalModificacion(string id)
        {
            Session["IdEmpleado"] = id;
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Sucursales.Where(x => x.Estatus == true).ToList());
            }

        }

        public ActionResult Edit(int id)
        {
            Empleados emp = new Empleados();
            string idEmpleado = Session["IdEmpleado"].ToString();
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                emp=dbmodel.Empleados.Where(x => x.IdEmpleado == idEmpleado).FirstOrDefault();
            }

            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                var consulta = from dato in dbmodel.Sucursales
                               where dato.IdSucursal == id
                               select dato;
                ViewBag.NombreSucursal = consulta.FirstOrDefault().Nombre;
                Session["Sucursal"] = emp.Sucursal;
                return View(emp);
            }
        }

        [HttpPost]
        public ActionResult Edit(string id, Empleados empleado)
        {
            try
            {
                empleado.IdEmpleado = Session["IdEmpleado"].ToString();
                empleado.Estatus = true;
                empleado.Sucursal = (Sucursales) Session["Sucursal"];

                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Entry(empleado).State = EntityState.Modified;
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index", "Empleados");
            }
            catch
            {
                return View();
            }
        }

        

        public ActionResult Details(string id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Empleados.Where(x => x.IdEmpleado == id).FirstOrDefault());
            }
        }

        public ActionResult Delete(string id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Empleados.Where(x => x.IdEmpleado == id).FirstOrDefault());
            }
        }

        [HttpPost]
        public ActionResult Delete(string id, Empleados empleado)
        {
            try
            {
                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    empleado = dbmodel.Empleados.Where(x => x.IdEmpleado == id).FirstOrDefault();
                    empleado.Estatus = false;
                    dbmodel.Entry(empleado).State = EntityState.Modified;
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index", "Empleados");
            }
            catch
            {
                return View();
            }
        }
    }
}