﻿using Proyecto_SGPF.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto_SGPF.Controllers
{
    public class ProductosController : Controller
    {
        // GET: Productos
        public ActionResult Index()
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Productos.Where(x => x.Estatus == true).ToList());
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Productos producto)
        {
            try
            {
                producto.Estatus = true;

                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Productos.Add(producto);
                    dbmodel.SaveChanges();
                }

                return RedirectToAction("Index", "Productos");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int? id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Productos.Where(x => x.IdProducto == id).FirstOrDefault());
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, Productos producto)
        {
            try
            {
                producto.IdProducto = id;
                producto.Estatus = true;
                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Entry(producto).State = EntityState.Modified;
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index", "Productos");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Details(int id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Productos.Where(x => x.IdProducto == id).FirstOrDefault());
            }
        }

        public ActionResult Delete(int id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Productos.Where(x => x.IdProducto == id).FirstOrDefault());
            }
        }

        [HttpPost]
        public ActionResult Delete(int id, Productos producto)
        {
            try
            {
                producto.IdProducto = id;
                producto.Estatus = false;
                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Entry(producto).State = EntityState.Modified;
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index", "Productos");
            }
            catch
            {
                return View();
            }
        }
    }
}