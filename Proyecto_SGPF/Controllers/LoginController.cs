﻿using Proyecto_SGPF.Models;
using Proyecto_SGPF.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto_SGPF.Controllers
{
    public class LoginController : Controller
    {
        LoginServices serviciosLogin;
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Empleados empleado)
        {

            ViewBag.Mensaje = "";
            serviciosLogin = new LoginServices();
            var empleados = serviciosLogin.VerificarUsuario(empleado);
            if (empleados.Count() > 0)
            {
                Session["User"] = empleados.FirstOrDefault();
                return RedirectToAction("Menu");
            }
            else
            {
                ViewBag.Mensaje = "Usuario no encontrado";
                return View();
            }
        }

        public ActionResult Menu()
        {
            try
            {
                Empleados empleados = (Empleados)Session["User"];
                return View(empleados);
            }
            catch
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult Salir()
        {
            Session["User"] = null;
            return RedirectToAction("Login");
        }
    }
}