﻿using Proyecto_SGPF.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto_SGPF.Controllers
{
    public class SucursalesController : Controller
    {
        // GET: Sucursales
        public ActionResult Index()
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Sucursales.Where(x => x.Estatus == true).ToList());
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        // POST: Sucursales/Create
        [HttpPost]
        public ActionResult Create(Sucursales sucursal)
        {
            try
            {
                sucursal.Estatus = true;

                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Sucursales.Add(sucursal);
                    dbmodel.SaveChanges();
                }

                return RedirectToAction("Index", "Sucursales");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int? id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Sucursales.Where(x => x.IdSucursal == id).FirstOrDefault());
            }
        }

        [HttpPost]
        public ActionResult Edit(int id, Sucursales sucursal)
        {
            try
            {
                sucursal.IdSucursal = id;
                sucursal.Estatus = true;
                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Entry(sucursal).State = EntityState.Modified;
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index", "Sucursales");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Details(int id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Sucursales.Where(x => x.IdSucursal == id).FirstOrDefault());
            }
        }

        public ActionResult Delete(int id)
        {
            using (PasteleriaContext dbmodel = new PasteleriaContext())
            {
                return View(dbmodel.Sucursales.Where(x => x.IdSucursal == id).FirstOrDefault());
            }
        }

        [HttpPost]
        public ActionResult Delete(int id, Sucursales sucursal)
        {
            try
            {
                sucursal.IdSucursal = id;
                sucursal.Estatus = false;
                using (PasteleriaContext dbmodel = new PasteleriaContext())
                {
                    dbmodel.Entry(sucursal).State = EntityState.Modified;
                    dbmodel.SaveChanges();
                }
                return RedirectToAction("Index", "Sucursales");
            }
            catch
            {
                return View();
            }
        }
    }
}